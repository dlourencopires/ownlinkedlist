﻿using System;

namespace OwnLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = { "c#", "php", "python", "java", "javascript", "sql" };
            MyOwnLinkedList myOwnLinkedList = new MyOwnLinkedList(words);

            try
            {
                Console.WriteLine($"First: {myOwnLinkedList.First()}");
                Console.WriteLine($"Next: {myOwnLinkedList.Next()}");
                Console.WriteLine($"Next: {myOwnLinkedList.Next()}");
                Console.WriteLine($"Previous: {myOwnLinkedList.Previous()}");
                Console.WriteLine($"Last: {myOwnLinkedList.Last()}");
                myOwnLinkedList.Add(value: "C++");
                Console.WriteLine($"Last after simple Add: {myOwnLinkedList.Last()}");
                Console.WriteLine($"Previous: {myOwnLinkedList.Previous()}");
                Console.WriteLine($"Next: {myOwnLinkedList.Next()}");
                Console.WriteLine($"First: {myOwnLinkedList.First()}");
                Console.WriteLine($"Last: {myOwnLinkedList.Last()}");
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine($"An error occured: {e.Message}"); 
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occured: {e.Message}");
            }

            Console.ReadKey();
        }
    }
}
