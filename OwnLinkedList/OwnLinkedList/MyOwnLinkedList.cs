﻿using System;
using System.Linq;

namespace OwnLinkedList
{
    internal class MyOwnLinkedList
    {
        private MyOwnLinkedItem[] _list;
        private int _currentIndex = 0;
        private int _mySize = 0;

        public MyOwnLinkedList(string[] words)
        {
            _list = new MyOwnLinkedItem[100000];

            for (var i=0; i < words.Length; i++)
            {
                MyOwnLinkedItem myOwnLinkedItem = new MyOwnLinkedItem(value: words[i],
                                                                      address: i);
                _list[i] = myOwnLinkedItem;
                _mySize++;
            }
        }

        /// <summary>
        /// Add at the final of the array
        /// </summary>
        /// <param name="value"></param>
        public void Add(string value)
        {
            MyOwnLinkedItem myOwnLinkedItem = new MyOwnLinkedItem(value: value,
                                                                  address: _mySize);
            _list[_mySize] = myOwnLinkedItem;
            _mySize++;
        }

        /// <summary>
        /// Gets the first element
        /// </summary>
        /// <returns></returns>
        public string First()
        {
            _currentIndex = 0;
            return Get(index: 0);
        }

        /// <summary>
        /// Gets the next element
        /// </summary>
        /// <returns></returns>
        public string Next()
        {
            _currentIndex++;
            return Get(index: _currentIndex);
        }

        /// <summary>
        /// Gets the previous element
        /// </summary>
        /// <returns></returns>
        public string Previous()
        {
            _currentIndex--;
            return Get(index: _currentIndex);
        }

        /// <summary>
        /// Gets the last element
        /// </summary>
        /// <returns></returns>
        public string Last()
        {
            _currentIndex = _mySize -1;
            return Get(index: _list[_currentIndex].Address);
        }

        /// <summary>
        /// Gets an element in one particular index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private string Get(int index)
        {
            MyOwnLinkedItem myOwnLinkedItem = _list
                .Where(x => x != null && x.Address.Equals(index))
                .FirstOrDefault();

            if(myOwnLinkedItem == null)
            {
                throw new NullReferenceException("You are already in the last one!");
            }

            return myOwnLinkedItem.Value;
        }
    }
}