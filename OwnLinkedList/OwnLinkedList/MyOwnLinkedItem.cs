﻿namespace OwnLinkedList
{
    class MyOwnLinkedItem
    {
        public string Value { get; set; }
        public int Address { get; set; }

        public MyOwnLinkedItem(string value, int address)
        {
            Value = value;
            Address = address;
        }
    }
}
